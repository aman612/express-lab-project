import express from 'express'
const router = express.Router()
const items = [{id: 1, task: "buy groceries"}, {id:2, task: "clean room"}]

router.get('/:id', (req, res) => {
    res.send(`You just requested item that should be with id ${req.params.id}`)
})


router.get('/', (req, res) => {
    res.send(items)
})

router.post('/', (req, res) => {

    const {id,task} = req.body
    if(!id || !task){
        return res.status(400).json({error:"invalid"}) 
    }
    items.push(req.body)
    return res.status(201).send(req.body)
})

export default router
